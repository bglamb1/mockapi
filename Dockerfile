FROM golang:1.16-alpine
RUN mkdir /mockapi
RUN cd /mockapi
RUN go mod init mockapi/mockapi
COPY mockapi.go ./
RUN go build -o /mockapi
EXPOSE 8080
CMD [ "/mockapi" ]
