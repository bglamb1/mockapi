package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/callbackend1", backend1Handler)
	http.HandleFunc("/callbackend2", backend2Handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func backend1Handler(w http.ResponseWriter, r *http.Request) {
	be1 := os.Getenv("BACKEND1")
	resp, err := http.Get("https://" + be1 + ".typicode.com/posts")
	if err != nil {
		log.Fatalln(err)
	}
	//We Read the response body on the line below.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	//Convert the body to type string
	sb := string(body)
	fmt.Fprintf(w, sb)
}

func backend2Handler(w http.ResponseWriter, r *http.Request) {
	be2 := os.Getenv("BACKEND2")
	resp, err := http.Get("https://" + be2 + ".typicode.com/posts")
	if err != nil {
		log.Fatalln(err)
	}
	//We Read the response body on the line below.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	//Convert the body to type string
	sb := string(body)
	fmt.Fprintf(w, sb)
}

